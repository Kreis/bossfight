#ifndef _SCENE_H_
#define _SCENE_H_

#include <iostream>
#include <functional>
#include <memory>
#include <map>

#include "Resources.h"
#include "GlobalConfig.h"
#include "Physics.h"
#include "Player.h"
#include "StageGeneric.h"
#include "StageTraining.h"

class Scene {
public:
  Scene();
  ~Scene();

  void init();
  void update(float deltatime, sf::RenderWindow* rwindow);
  void draw();

private:
  std::unique_ptr<Player> player;
  // Stages
  std::map<std::string, std::unique_ptr<StageGeneric> > stages;
  StageGeneric* currentStage;

  Vec<int32_t> currentScreenSize;
  Sprite banner1;
  Sprite banner2;
  bool drawBanners;
};

#endif
