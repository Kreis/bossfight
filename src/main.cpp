#include <iostream>

#include "World.h"

int main(int argc, char** argv) {

  std::cout << "Start program" << std::endl;

  World world;
  world.start();

  return 0;
}

