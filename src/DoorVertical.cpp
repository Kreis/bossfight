#include "DoorVertical.h"


DoorVertical::DoorVertical() {}
DoorVertical::~DoorVertical() {}

void DoorVertical::init(Rect<int32_t> bounds) {
  Resources::get()->loadAnimator("data/animators/doorVertical.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/doorVertical.json");
  animator->runAnimation("idle");

  bodyId = Physics::get()->generateBody(
    bounds.x, bounds.y, bounds.w, bounds.h, true);
  Body* body = Physics::get()->getBody(bodyId);
  body->setType(9);

  this->bounds = bounds;
  limitTop = bounds.y - bounds.h;
  limitBot = bounds.y;
}

void DoorVertical::update(float deltatime, bool emblemOn) {

  Body* body = Physics::get()->getBody(bodyId);
  if (emblemOn) {
    body->move(0, -200.0 * deltatime);   
  } else {
    body->move(0, 200.0 * deltatime);   
  }

  if (body->getY() < limitTop) {
    body->setPositionY(limitTop);
  }
  if (body->getY() > limitBot) {
    body->setPositionY(limitBot);
  }
}

void DoorVertical::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/doorVertical.json");
  Body* body = Physics::get()->getBody(bodyId);

  animator->bounds.x = body->getX().get();
  animator->bounds.y = body->getY().get();
  animator->draw();
}

