#include "SpriteBatch.h"
#include "Resources.h"

SpriteBatch::SpriteBatch() {}
SpriteBatch::~SpriteBatch() {}

std::unique_ptr<SpriteBatch> SpriteBatch::instancePtr = nullptr;

SpriteBatch* SpriteBatch::get() {

  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<SpriteBatch>(new SpriteBatch());
  }

  return instancePtr.get();
}


void SpriteBatch::init(std::vector<std::string> _texturePriorities) {
  texturePriorities = _texturePriorities;

  std::vector<sf::Vertex> v;
  v.reserve(200);
  vecVertex.resize(texturePriorities.size(), v);
  isFixVecVertex.resize(vecVertex.size(), false);
  isEnableVecVertex.resize(vecVertex.size(), true);
  cameraVecVertex.resize(vecVertex.size(), Vec<float>());
  shaderInVecVertex.resize(vecVertex.size(), "");
  isNullTextureVecVertex.resize(vecVertex.size(), false);
  isCameraFreeVecVertex.resize(vecVertex.size(), false);
}

void SpriteBatch::drawSprite(Sprite* sprite) {

  if (texturePriorities.size() == 0) {
    std::cout << "ERROR: draw sprite without texture priority" << std::endl;
  }

  int32_t index = getTexturePriorityId(sprite->textureName);

  if (index == -1) {
    std::cout << "ERROR: texture name " << sprite->textureName
      << " not found" << std::endl;
  }

  sf::Vertex a;
  a.position.x = sprite->bounds.x;
  a.position.y = sprite->bounds.y;
  a.texCoords.x = sprite->textureRect.x;
  a.texCoords.y = sprite->textureRect.y;
  a.color = sf::Color(
    sprite->color.x,
    sprite->color.y,
    sprite->color.w,
    sprite->color.h);

  sf::Vertex b;
  b.position.x = sprite->bounds.x + sprite->bounds.w;
  b.position.y = sprite->bounds.y;
  b.texCoords.x = sprite->textureRect.x + sprite->textureRect.w;
  b.texCoords.y = sprite->textureRect.y;
  b.color = a.color;

  sf::Vertex c;
  c.position.x = sprite->bounds.x + sprite->bounds.w;
  c.position.y = sprite->bounds.y + sprite->bounds.h;
  c.texCoords.x = sprite->textureRect.x + sprite->textureRect.w;
  c.texCoords.y = sprite->textureRect.y + sprite->textureRect.h;
  c.color = a.color;

  sf::Vertex d;
  d.position.x = sprite->bounds.x;
  d.position.y = sprite->bounds.y + sprite->bounds.h;
  d.texCoords.x = sprite->textureRect.x;
  d.texCoords.y = sprite->textureRect.y + sprite->textureRect.h;
  d.color = a.color;
  
  if (sprite->rotation != 0.0) {
    float midX = (b.position.x - a.position.x) / 2.0;
    float midY = (d.position.y - b.position.y) / 2.0;
    float toWorldX = a.position.x + midX;
    float toWorldY = a.position.y + midY;
    float ang = sprite->rotation;
    float ax = -midX;
    float ay = -midY;
    float nx = ax * cos(ang) - ay * sin(ang);
    float ny = ax * sin(ang) + ay * cos(ang);
    a.position.x = nx + toWorldX;
    a.position.y = ny + toWorldY;

    float bx = midX;
    float by = -midY;
    nx = bx * cos(ang) - by * sin(ang);
    ny = bx * sin(ang) + by * cos(ang);
    b.position.x = nx + toWorldX;
    b.position.y = ny + toWorldY;

    float cx = midX;
    float cy = midY;
    nx = cx * cos(ang) - cy * sin(ang);
    ny = cx * sin(ang) + cy * cos(ang);
    c.position.x = nx + toWorldX;
    c.position.y = ny + toWorldY;

    float dx = -midX;
    float dy = midY;
    nx = dx * cos(ang) - dy * sin(ang);
    ny = dx * sin(ang) + dy * cos(ang);
    d.position.x = nx + toWorldX;
    d.position.y = ny + toWorldY;
  }
  if (sprite->horizontalSwitch) {
    std::swap(a.position, b.position);
    std::swap(c.position, d.position);
  }
  if (sprite->verticalSwitch) {
    std::swap(a.position, d.position);
    std::swap(b.position, c.position);
  }

  vecVertex[ index ].push_back(a);
  vecVertex[ index ].push_back(b);
  vecVertex[ index ].push_back(c);
  vecVertex[ index ].push_back(d);

}

void SpriteBatch::fixVecVertex(std::string _textureName,
    std::vector<sf::Vertex>* _vecVertex) {

  std::cout << "Sprite batch fix vertex for textureName "
    << _textureName << std::endl;

  int32_t index = getTexturePriorityId(_textureName);

  if (index == -1) {
    std::cout << "ERROR: fixVertex not found texture "
      << _textureName << std::endl;
  }
  
  vecVertex[ index ] = *_vecVertex; 
  isFixVecVertex[ index ] = true;
}

void SpriteBatch::setEnableVecVertex(std::string _textureName, bool enable) {
  
  std::cout << "set vec vertex enable to " << (enable ? "true" : "false")
    << std::endl;

  int32_t index = getTexturePriorityId(_textureName);
  if (index == -1) {
    std::cout << "ERROR: enableVertex not found texture "
      << _textureName << std::endl;
  }

  isEnableVecVertex[ index ] = enable;
}

void SpriteBatch::clearVecVertex(std::string _textureName) {
  std::cout << "clear vec vertex " << _textureName << std::endl;

  int32_t index = getTexturePriorityId(_textureName);
  if (index == -1) {
    std::cout << "ERROR: clearVecVertex not found texture "
      << _textureName << std::endl;
  }

  vecVertex[ index ].clear();
  cameraVecVertex[ index ] = Vec<float>();
}

void SpriteBatch::setShaderToVecVertex(
  std::string _textureName, std::string _shaderName) {

  std::cout << "set shader to vecVertex " << _textureName << std::endl;

  int32_t index = getTexturePriorityId(_textureName);
  if (index == -1) {
    std::cout << "ERROR: setShaderToVecVertex not found texture "
      << _textureName << std::endl;
  }
  shaderInVecVertex[ index ] = _shaderName;
}

void SpriteBatch::setNullTextureVecVertex(std::string _textureName) {
  std::cout << "set null texture vecVertex " << _textureName << std::endl;

  int32_t index = getTexturePriorityId(_textureName);
  if (index == -1) {
    std::cout << "ERROR: setNullTextureVecVertex not found texture "
      << _textureName << std::endl;
  }

  isNullTextureVecVertex[ index ] = true;
}

void SpriteBatch::setCameraFreeVecVertex(
  std::string _textureName, bool enable) {

  std::cout << "set camera free vecVertex " << _textureName << std::endl;
  int32_t index = getTexturePriorityId(_textureName);
  if (index == -1) {
    std::cout << "ERROR: setCameraFreeVecVertex not found texture "
      << _textureName << std::endl;
  }

  isCameraFreeVecVertex[ index ] = enable;
}

void SpriteBatch::flush(Window* window) {

  if (texturePriorities.size() == 0) {
    std::cout << "ERROR: draw sprite without texture priority" << std::endl;
  }

  window->getSfRenderWindow()->clear();

  for (int32_t i = 0; i < texturePriorities.size(); i++) {

    if (vecVertex[ i ].size() == 0) {
      continue;
    }
    if ( ! isEnableVecVertex[ i ]) {
      continue;
    }

    if (isNullTextureVecVertex[ i ]) {
      rs.texture = nullptr;
    } else {
      rs.texture = Resources::get()->getTexture(texturePriorities[ i ]);
    }

    if (shaderInVecVertex[ i ].length() > 0) {
      rs.shader = Resources::get()->getShader(shaderInVecVertex[ i ]);
    } else {
      rs.shader = nullptr;
    }

    // update camera offset
    int32_t diffX = Camera::get()->bounds.x - Camera::get()->banners.x -
      cameraVecVertex[ i ].x;
    int32_t diffY = Camera::get()->bounds.y - Camera::get()->banners.y -
      cameraVecVertex[ i ].y;

    if ( ! isCameraFreeVecVertex[ i ] )
      for (sf::Vertex& vertex : vecVertex[ i ]) {

      vertex.position.x -= diffX;
      vertex.position.y -= diffY;
    }

    cameraVecVertex[ i ].x += diffX;
    cameraVecVertex[ i ].y += diffY;

    window->getSfRenderWindow()->draw(
      &vecVertex[ i ][0],
      vecVertex[ i ].size(),
      sf::PrimitiveType::Quads,
      rs);

    if ( ! isFixVecVertex[ i ]) {
      vecVertex[ i ].clear();
      cameraVecVertex[ i ] = Vec<float>();
    }
  }

  window->getSfRenderWindow()->display();
}


int32_t SpriteBatch::getTexturePriorityId(std::string textureName) {

  int32_t index = -1;
  for (int32_t i = 0; i < texturePriorities.size(); i++) {

    if (textureName == texturePriorities[ i ]) {
      index = i;
      break;
    }
  }
 
  return index;
}






