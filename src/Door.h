#ifndef _DOOR_H_
#define _DOOR_H_

#include "Physics.h"
#include "SpriteBatch.h"
#include "Animator.h"
#include "Resources.h"

class Door {
public:
  Door();
  ~Door();

  void init(Rect<int32_t> bounds);
  void update(float deltatime, bool buttonPushed);
  void draw();

  int32_t body;
  Rect<int32_t> bounds;
  int32_t limitLeft;
  int32_t limitRight;
};


#endif
