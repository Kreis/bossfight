#ifndef _SHARED_TYPES_H_
#define _SHARED_TYPES_H_

#ifdef _MSC_VER
#if _MSC_VER >= 1600
#include <cstdint>
#else
typedef __int8              int8_t;
typedef __int16             int16_t;
typedef __int32             int32_t;
typedef __int64             int64_t;
typedef unsigned __int8     uint8_t;
typedef unsigned __int16    uint16_t;
typedef unsigned __int32    uint32_t;
typedef unsigned __int64    uint64_t;
#endif
#elif __GNUC__ >= 3
#include <cstdint>
#endif
#include <assert.h>
#include <iostream>

template <class T>
class Vec {
public:
  T x{};
  T y{};

  bool operator == (const Vec<T>& other) const {
    return this->x == other.x && this->y == other.y;
  }
  bool operator != (const Vec<T>& other) const {
    return ! (*this == other);
  }
  Vec<T> operator * (float v) const {
    Vec<T> n;
    n.x = this->x * v;
    n.y = this->y * v;
    return n;
  }
  Vec& operator = (const Vec<T>& other) {
    this->x = other.x;
    this->y = other.y;
    return *this;
  }
};

template <class T>
class Rect {
public:
  T x{};
  T y{};
  T w{};
  T h{};

  inline bool collide(const Rect<T>& other) {
    return ! (this->x >= other.x + other.w ||
      this->x + this->w <= other.x ||
      this->y >= other.y + other.h ||
      this->y + this->h <= other.y);
  }

  bool operator == (const Rect<T>& other) const {
    return this->x == other.x && this->y == other.y
      && this->w == other.w && this->h == other.h;
  }
  bool operator != (const Rect<T>& other) const {
    return ! (*this == other);
  }
  Rect& operator = (const Rect<T>& other) {
    this->x = other.x;
    this->y = other.y;
    this->w = other.w;
    this->h = other.h;
    return *this;
  }

  T& r = x;
  T& g = y;
  T& b = w;
  T& a = h;
};

class vf {
public:
  vf() : n(0), d(1) {}
  vf(float v) : n(0), d(1) { fromFloat(v); }
  vf(const vf& v) : n(v.n), d(v.d) { simplify(); }
  vf(int64_t n, int64_t d) : n(n), d(d) { simplify(); }
  ~vf() {}
  float get() const {
    return (float)n / (float)d;
  }

  // aritmetic
  vf operator - () const {
    return vf(n * -1, d);
  }
  friend vf operator - (float other, const vf& me) {
    return vf(other) - me;
  }
  vf operator - (float other) const {
    return (*this) - vf(other);
  }
  vf operator - (const vf& other) const {
    const int64_t l = getLcm(d, other.d);
    return vf(n * (l / d) - (other.n * (l / other.d)), l);
  }
  friend vf operator + (float other, const vf& me) {
    return vf(other) + me;
  }
  vf operator + (float other) const {
    return (*this) + vf(other);
  }
  vf operator + (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return vf(n * (lcm / d) + (other.n * (lcm / other.d)), lcm);
  }
  friend vf operator / (float other, const vf& me) {
    return vf(other) / me;
  }
  vf operator / (float other) const {
    return (*this) / vf(other);
  }
  vf operator / (const vf& other) const {
    return vf(n * other.d, d * other.n);
  }
  friend vf operator * (float other, vf& me) {
    return vf(other) * me;
  }
  vf operator * (float other) {
    return (*this) * vf(other);
  }
  vf operator * (const vf& other) {
    return vf(n * other.n, d * other.d);
  }
  void operator -= (float other) {
    (*this) -= vf(other);
  }
  void operator -= (const vf& other) {
    const int64_t lcm = getLcm(d, other.d);
    n = (n * (lcm / d)) - (other.n * (lcm / other.d));
    d = lcm;
  }
  void operator += (float other) {
    (*this) += vf(other);
  }
  void operator += (const vf& other) {
    const int64_t lcm = getLcm(d, other.d);
    n = (n * (lcm / d)) + (other.n * (lcm / other.d));
    d = lcm;
  }
  void operator /= (float other) {
    (*this) /= vf(other);
  }
  void operator /= (const vf& other) {
    n *= other.d;
    d *= other.n;
    simplify();
  }
  void operator *= (float other) {
    (*this) *= vf(other);
  }
  void operator *= (const vf& other) {
    n *= other.n;
    d *= other.d;
    simplify();
  }

  // boolean
  friend bool operator == (float other, const vf& me) {
    return vf(other) == me;
  }
  bool operator == (float other) const {
    return (*this) == vf(other);
  }
  bool operator == (const vf& other) const {
    return n == other.n && d == other.d;
  }
  friend bool operator != (float other, const vf& me) {
    return vf(other) != me;
  }
  bool operator != (float other) const {
    return (*this) != vf(other);
  }
  bool operator != (const vf& other) const {
    return n != other.n || d != other.d;
  }
  friend bool operator < (float other, const vf& me) {
    return vf(other) < me;
  }
  bool operator < (float other) const {
    return (*this) < vf(other);
  }
  bool operator < (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) < other.n * (lcm / other.d);
  }
  friend bool operator > (float other, const vf& me) {
    return vf(other) > me;
  }
  bool operator > (float other) const {
    return (*this) > vf(other);
  }
  bool operator > (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) > other.n * (lcm / other.d);
  }
  friend bool operator >= (float other, const vf& me) {
    return vf(other) >= me;
  }
  bool operator >= (float other) const {
    return (*this) >= vf(other);
  }
  bool operator >= (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) >= other.n * (lcm / other.d);
  }
  friend bool operator <= (float other, const vf& me) {
    return vf(other) <= me;
  }
  bool operator <= (float other) const {
    return (*this) <= vf(other);
  }
  bool operator <= (const vf& other) const {
    const int64_t lcm = getLcm(d, other.d);
    return n * (lcm / d) <= other.n * (lcm / other.d);
  }
  friend std::ostream& operator <<(std::ostream& os, const vf& v) {
    os << v.n << " / " << v.d << " = " << v.get();
    return os;
  }

private:
  int64_t n;
  int64_t d;
  void fromFloat(float v) {
    n = (int64_t)v;
    uint8_t cont = 0;
    while (cont < 4 && v - (float)n != 0) {
      cont++;
      v *= 10;
      n = (int64_t)v;
    }
    while (cont-- > 0) {
      d *= 10;
    }
    simplify();
  }

  inline int64_t getGcd(int64_t a, int64_t b) const {
    if (b == 0) return a;
    return getGcd(b, a % b);
  }

  inline int64_t getLcm(int64_t a, int64_t b) const {
    a = a < 0 ? -a : a;
    b = b < 0 ? -b : b;
    return (a * b) / getGcd(a < 0 ? -a : a, b < 0 ? -b : b);
  }

  inline void simplify() {
    const int64_t gcd = getGcd(n, d);
    this->n = n / gcd;
    this->d = d / gcd;
    n = d < 0 ? -n : n;
    d = d < 0 ? -d : d;
  }
};

#endif
