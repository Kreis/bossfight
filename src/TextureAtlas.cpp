#include "TextureAtlas.h"

TextureAtlas::TextureAtlas() {
  pTextureRect.resize(60);
}
TextureAtlas::~TextureAtlas() {}

void TextureAtlas::createPiece(std::string name) {
  if (dir[name] != 0) {
    std::cout << "ERROR: createPiece already use name " << name << std::endl;
    return;
  }
  index++;
  dir[name] = index;
}

void TextureAtlas::setFrame(std::string name, Rect<int32_t> rect) {

  if (dir[name] == 0) {
    std::cout << "ERROR: setFrame not Found Piece texture" << std::endl;
    return;
  }

  int32_t i = dir[name];
  pTextureRect[ i ] = rect;
}

Rect<int32_t> TextureAtlas::getPieceRect(std::string name) {
  if (dir[name] == 0) {
    std::cout << "ERROR: getPieceRectX not Found Piece texture" << std::endl;
  }

  return pTextureRect[ dir[name] ];
}


