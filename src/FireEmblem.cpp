#include "FireEmblem.h"

FireEmblem::FireEmblem() {}
FireEmblem::~FireEmblem() {}

void FireEmblem::init(Vec<int32_t> pos) {
  Resources::get()->loadAnimator("data/animators/fireEmblem.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/fireEmblem.json");
  sessionId = animator->getNewSession();
  animator->runAnimation("off", sessionId);

  on = false;
  delayOn = 0.0;
  this->pos = pos;

  bounds.x = pos.x;
  bounds.y = pos.y;
  bounds.w = animator->bounds.w;
  bounds.h = animator->bounds.h;

  Resources::get()->loadSound("data/sounds/emblemOn.wav");
}

void FireEmblem::update(float deltatime, std::vector<FireBall>& fbs) {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/fireEmblem.json");
 
  bool newOn = false;
  for (FireBall& f : fbs) {
    if ( ! f.alive) {
      continue;
    }
    if (f.getBounds().collide(bounds)) {
      newOn = true;
      break;
    }
  }

  // spending time without fire
  if (on && ! newOn) {
    delayOn -= deltatime;
  }

  // if new state and without delay to spend
  if (newOn != on && delayOn <= 0.0) {
    on = newOn;
    if (on) {
      // again on recharge delay
      float vol = GlobalConfig::get()->volume;
      Resources::get()->getSound("data/sounds/emblemOn.wav")->setVolume(vol * vol * 100.0);
      Resources::get()->getSound("data/sounds/emblemOn.wav")->play();
      delayOn = 2.0;
      animator->runAnimation("on", sessionId);
    } else {
      animator->runAnimation("off", sessionId);
    }
  }

  animator->update(deltatime, sessionId);
}

void FireEmblem::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/fireEmblem.json");
  animator->bounds.x = pos.x;
  animator->bounds.y = pos.y;
  animator->draw(sessionId);
}


