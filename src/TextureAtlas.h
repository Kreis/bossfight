#ifndef _TEXTURE_ATLAS_H_
#define _TEXTURE_ATLAS_H_

#include <iostream>
#include <memory>
#include <vector>
#include <map>

#include "sharedtypes.h"

class TextureAtlas {
friend class Resources;
public:
  ~TextureAtlas();

  void createPiece(std::string name);
  void setFrame(std::string name, Rect<int32_t> rect);

  Rect<int32_t> getPieceRect(std::string name);

  std::string textureName;

private:
  TextureAtlas();
  std::map<std::string, int32_t> dir;
  int32_t index = 0;

  std::vector<Rect<int32_t> > pTextureRect;
};

#endif
