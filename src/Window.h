#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <iostream>
#include <memory>

#include <SFML/Graphics.hpp>

#include "InputState.h"

class Window {
public:
  Window();
  ~Window();

  void start(std::string name, int32_t width, int32_t height);

  void update();

  bool isOpen();

  void setIcon(std::string name);

  void close();

  sf::RenderWindow* getSfRenderWindow();

private:
  std::unique_ptr<sf::RenderWindow> sfRenderWindow = nullptr;
  sf::Image icon;
};

#endif
