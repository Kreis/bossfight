#ifndef _NODE_ANIMATOR_H_
#define _NODE_ANIMATOR_H_

#include <iostream>
#include <vector>
#include <map>

class NodeAnimator {
friend class Resources;
public:
  ~NodeAnimator() {}

  std::string animationName;
  std::vector<int32_t> nodeAnimators;
  std::vector<std::vector<
    std::map<std::string, std::string> > > conditions;

private:
  NodeAnimator() {}
};

#endif
