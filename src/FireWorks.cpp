#include "FireWorks.h"

FireWorks::FireWorks() {}
FireWorks::~FireWorks() {}

void FireWorks::init(Rect<int32_t> goal, Rect<int32_t> rectFireWorks) {
  std::cout << "init fireworks" << std::endl;
  win = false;
  bounds = rectFireWorks;
  goalBounds = goal;

  Resources::get()->loadShader("data/shaders/fireworks");
  SpriteBatch::get()->setShaderToVecVertex(
    "FIREWORKS", "data/shaders/fireworks");  

  sprite.textureName = "FIREWORKS";
  sprite.bounds = rectFireWorks;
  sprite.textureRect = rectFireWorks; 
  sprite.textureRect.x = 0;
  sprite.textureRect.y = 0;

  sf::Shader* shader = Resources::get()->getShader(
    "data/shaders/fireworks");
  shader->setParameter("u_resolution",
    sf::Vector2f(
      Camera::get()->bounds.w,
      Camera::get()->bounds.h));

  Resources::get()->loadSound("data/sounds/winning.wav");

  time = 0;
}

void FireWorks::update(float deltatime, Player* player) {
  Rect<int32_t> rectPlayer = player->getBounds();

  if ( ! win && rectPlayer.collide(goalBounds)) {
    win = true;
    std::cout << "win" << std::endl;
    float vol = GlobalConfig::get()->volume;
    Resources::get()->getSound("data/sounds/winning.wav")->setVolume(vol * vol * 100);
    Resources::get()->getSound("data/sounds/winning.wav")->play();
  }

  if (win) {
    time += deltatime;
    sf::Shader* shader = Resources::get()->getShader(
      "data/shaders/fireworks");
    shader->setParameter("u_time", time);
  }
}

void FireWorks::draw() {
  if (win) {
    SpriteBatch::get()->drawSprite(&sprite);
  }
}


