#include "Spiral.h"


Spiral::Spiral() {}
Spiral::~Spiral() {}

void Spiral::init() {
  active = false;
  
  Resources::get()->loadAnimator("data/animators/spiral.json");
  Animator* animator = Resources::get()->getAnimator(
    "data/animators/spiral.json");
  animator->runAnimation("idle");
  bounds = animator->bounds;

  bodyIdSpace = Physics::get()->generateBody(
    0, 0, 96, 96, true);
  Body* body = Physics::get()->getBody(bodyIdSpace);
  body->setEnabled(false);
  body->setType(1);
}

void Spiral::putIt(Vec<float> pos) {
  bounds.x = pos.x;
  bounds.y = pos.y;
  
  Body* body = Physics::get()->getBody(bodyIdSpace);
  body->setPosition(bounds.x - 32, bounds.y - 32);
  active = true;
}

Vec<int32_t> Spiral::getIt() {
  active = false;
  return { bounds.x, bounds.y };
}

void Spiral::update(float deltatime, std::vector<FireBall>& fbs) {
  if ( ! active) {
    return;
  }

  for (FireBall& fb : fbs) {
    if ( ! fb.alive) {
      continue;
    }

    Rect<int32_t> fbbound = fb.getBounds();
    if (fbbound.collide(bounds)) {

      Body* body = Physics::get()->getBody(bodyIdSpace);
      body->setPosition(fbbound.x - 16, fbbound.y - 16);
      body->setEnabled(true);
      bool coll = Physics::get()->couldCollide(bodyIdSpace, 0, 0);
      body->setEnabled(false);
      
      if ( ! coll) {
        bounds.x = fbbound.x + 16;
        bounds.y = fbbound.y + 16;
      }
      break;
    }
  }


  Animator* animator = Resources::get()->getAnimator(
    "data/animators/spiral.json");
  animator->bounds.x = bounds.x;
  animator->bounds.y = bounds.y;
  animator->update(deltatime);
}

void Spiral::draw() {
  if ( ! active) {
    return;
  }
  Animator* animator = Resources::get()->getAnimator(
    "data/animators/spiral.json");
  animator->draw();
}

