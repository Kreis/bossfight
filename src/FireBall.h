#ifndef _FIREBALL_H_
#define _FIREBALL_H_

#include "Animator.h"
#include "Resources.h"
#include "GlobalConfig.h"
#include "Physics.h"
#include "sharedtypes.h"

class FireBall {
public:
  FireBall();
  ~FireBall();

  void init(Vec<int32_t> position, Vec<float> _direction);
  void updateAndDraw(float deltatime);

  Rect<int32_t> getBounds();
  Vec<float> direction;

  int32_t bodyId;
  bool alive;
  int32_t sessionId;
};

#endif
