#include "Button.h"


Button::Button() {}
Button::~Button() {}

void Button::init(Rect<int32_t> rect) {
  pushed = false;

  body = Physics::get()->generateBody(
    rect.x, rect.y, rect.w, rect.h, false);

  Resources::get()->loadAnimator("data/animators/button.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/button.json");
  animator->runAnimation("idle");
  animator->bounds.x = rect.x;
  animator->bounds.y = rect.y;

  buttonBounds = rect;
  timeShaking = 0.6;
  currentShaking = timeShaking;
  cameraMovedId = 0;

  Resources::get()->loadSound("data/sounds/buttonPush.wav");
}

void Button::update(float deltatime, Player* player) {

  Animator* animator =
    Resources::get()->getAnimator("data/animators/button.json");

  int32_t px = player->getBounds().x;
  int32_t pw = player->getBounds().w;
  if (pushed && 
    (px > buttonBounds.x + buttonBounds.w ||
    px + pw < buttonBounds.x)) {

    unpush();
  }

  int32_t botCoord = player->getBounds().y +
    player->getBounds().h;
  if ( ! pushed && botCoord == buttonBounds.y &&
    player->down && ! player->falling && ! player->jumping) {
    push();
    currentShaking = 0.0;
    sf::Sound* sound = Resources::get()->getSound("data/sounds/buttonPush.wav");
    float vol = GlobalConfig::get()->volume;
    sound->setVolume(vol * vol * 100.0);
    sound->play();
  }

  if (pushed) {
    animator->runAnimation("pushed");
  } else {
    animator->runAnimation("idle");
  }
  animator->update(deltatime);

  if (currentShaking < timeShaking) {
    currentShaking += deltatime;
    float x[] = { -2, 2, -2, 2, -2, 2, 2 };
    float y[] = { -13, 12, -14, 16, -18, 13, 0 };
    cameraMoved.x += x[cameraMovedId];
    cameraMoved.y += y[cameraMovedId];
    Camera::get()->bounds.x += x[cameraMovedId];
    Camera::get()->bounds.y += y[cameraMovedId];
    cameraMovedId = cameraMovedId == 6 ? 0 : cameraMovedId + 1;
  } else if (cameraMoved.x > 0.0 && cameraMoved.y > 0.0) {
    Camera::get()->bounds.x += cameraMoved.x;
    Camera::get()->bounds.y += cameraMoved.y;
    cameraMoved.x = 0.0;
    cameraMoved.y = 0.0;
  }
}

void Button::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/button.json");
  animator->draw();
}

void Button::push() {
  pushed = true;
  Body* b = Physics::get()->getBody(body);
  b->setPosition(-300, -300); // any place outside of the map
}

void Button::unpush() {
  pushed = false;
  Body* b = Physics::get()->getBody(body);
  b->setPosition(buttonBounds.x, buttonBounds.y);
}



