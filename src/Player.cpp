#include "Player.h"

Player::Player() {}
Player::~Player() {}

void Player::init() {

  std::cout << "Player init" << std::endl;

  Resources::get()->loadAnimator("data/animators/player.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/player.json");
  
  bodyId = Physics::get()->generateBody(
    animator->bounds.x,
    animator->bounds.y,
    animator->bounds.w,
    animator->bounds.h,
    /*isDynamic=*/true);
  Physics::get()->getBody(bodyId)->setType(1);

  spiral.init();
  
  jumping = false;
  falling = false;
  fireBall = false;
  down = false;
  flip = false;
  reappeared = false;

  animatorStatus["press left or right"] = "false";
  animatorStatus["collide down"] = "true";
  animatorStatus["type space"] = "false";
  animatorStatus["type x"] = "false";
  animatorStatus["press down"] = "false";
  animatorStatus["press up"] = "false";


  Resources::get()->loadSound("data/sounds/letZ.wav");
  Resources::get()->loadSound("data/sounds/rewind.wav");
  Resources::get()->loadSound("data/sounds/noZ.wav");
}

void Player::update(float deltatime) {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/player.json");
  Body* body = Physics::get()->getBody(bodyId);

  // move left right
  down = InputState::get()->isPressed(KeyButton::Down);
  bool right = InputState::get()->isPressed(KeyButton::Right);
  bool left = InputState::get()->isPressed(KeyButton::Left);
  bool up = InputState::get()->isPressed(KeyButton::Up);
  if (right != left && ! down & ! up) {
    if (right) {
      body->move(400.0 * deltatime, 0);
    } else {
      body->move(-400.0 * deltatime, 0);
    }
  }

  int32_t myPosX = (int32_t)body->getX().get();
  int32_t myPosY = (int32_t)body->getY().get();


  float vol = GlobalConfig::get()->volume;

  reappeared = false;
  // Z return
  bool zzz = InputState::get()->isTyped(KeyButton::Z);
  if (zzz && ! spiral.active) {
    spiral.putIt({(float)(myPosX + 32.0), (float)(myPosY + 32.0)});
    Resources::get()->getSound("data/sounds/letZ.wav")->setVolume(vol * vol * 100.0);
    Resources::get()->getSound("data/sounds/letZ.wav")->play();
  } else if (zzz && spiral.active) {
    reappeared = true;
    Vec<int32_t> spiralPos = spiral.getIt();

    int32_t oldPosX = body->getX().get();
    int32_t oldPosY = body->getY().get();

    Vec<int32_t> npos;
    npos.x = spiralPos.x - 32;
    npos.y = spiralPos.y - 32;
    body->setPosition(npos.x, npos.y);
    // check if we're colliding, then abort Z return
    if (Physics::get()->couldCollide(bodyId, 0, 0)) {
      body->setPosition(oldPosX, oldPosY);
      reappeared = false;
      Resources::get()->getSound("data/sounds/noZ.wav")->setVolume(vol * vol * 100.0);
      Resources::get()->getSound("data/sounds/noZ.wav")->play();
    }

    if (reappeared) {
      Resources::get()->getSound("data/sounds/rewind.wav")->setPitch(4.0);
      Resources::get()->getSound("data/sounds/rewind.wav")->setVolume(vol * vol * 100.0);
      Resources::get()->getSound("data/sounds/rewind.wav")->play();
    }
  }

  // hit the ground
  if (body->collidingDown && ! reappeared) {
    jumping = false;
    falling = false;
  }

  // if jumping calculate parabola
  if (InputState::get()->isTyped(KeyButton::Space) &&
    ! jumping && ! falling && ! down) {
    std::cout << "jumping" << std::endl;
    jumping = true;
    parabola.init(
      body,
      /*time to peak=*/0.4,
      /*height=*/96,
      /*current time=*/0,
      TypeParabola::VERTICAL_INVERTED);
  }

  // if falling calculate parabola
  bool notCollidedown = ! Physics::get()->couldCollide(bodyId, 0, 1);
  if (notCollidedown &&
    ((! falling && ! jumping) || reappeared)) {

    std::cout << "falling" << std::endl;
    falling = true;
    down = false;
    parabola.init(
      body,
      /*time to peak=*/0.4,
      /*height=*/96,
      /*current time=*/0.4,
      TypeParabola::VERTICAL_INVERTED);
  }

  // update parabola
  if (jumping || falling) {
    bool parabolaOk = parabola.update(deltatime, body);
    if ( ! parabolaOk) {
      jumping = false;
    }
  }

  // flip sprite
  if (right != left) {
    flip = left;
    animator->setHorizontalSwitch(flip);
  }

  // fire ball
  fireBall = InputState::get()->isTyped(KeyButton::X) &&
    ! down && currentFireBallDelay > fireBallDelay;
  currentFireBallDelay += deltatime;

  if (fireBall) {
    int32_t idF = nextFireBall();
    currentFireBallDelay = 0.0;
  
    Vec<int32_t> firePosition;
    firePosition.x = myPosX + 16;
    firePosition.y = myPosY + 16;

    Vec<float> dir;
    float hypotenuse = 600.0;
    float leg = 424.26;
    // if diag
    if (up && left != right) {
      dir.x = leg;      
      dir.y = -leg;      
    }
    // if up
    if (up && left == right) {
      dir.x = 0.0;
      dir.y = -hypotenuse;
    }
    // if straight
    if ( ! up) {
      dir.x = hypotenuse;
      dir.y = 0.0;
    }
    dir.x = flip ? dir.x * -1.0 : dir.x;
    
    fireBalls[ idF ].init(
      firePosition,
      dir);
  }
  // update and draw fireball
  for (int32_t i = 0; i < fireBalls.size(); i++) {
    if (fireBalls[ i ].alive) {
      fireBalls[ i ].updateAndDraw(deltatime);
    } else if ( ! mapBuriedFireBalls[ i ]) {
      queuefireBalls.push(i);
      mapBuriedFireBalls[ i ] = true;
    }
  }
  
  // fix animator coords to physics body
  animator->bounds.x = (int32_t)body->getX().get();
  animator->bounds.y = (int32_t)body->getY().get();

  // update animator
  animatorStatus["collide down"] =
    Physics::get()->couldCollide(bodyId, 0, 1) ? "true" : "false";
  animatorStatus["press down"] =
    InputState::get()->isPressed(KeyButton::Down) ? "true" : "false";
  animatorStatus["type space"] =
    InputState::get()->isTyped(KeyButton::Space) ? "true" : "false";
   animatorStatus["type x"] =
    fireBall ? "true" : "false";
  animatorStatus["press left or right"] =
    right != left ? "true" : "false";
  animatorStatus["press up"] =
    up ? "true": "false";

  animator->update(deltatime, animatorStatus);
  spiral.update(deltatime, fireBalls);
}

void Player::draw() {

  Animator* animator =
    Resources::get()->getAnimator("data/animators/player.json");

  animator->draw();
  spiral.draw();
}


Rect<int32_t> Player::getBounds() {
  Body* body = Physics::get()->getBody(bodyId);
  Rect<int32_t> r;
  r.x = body->getX().get();
  r.y = body->getY().get();
  r.w = body->getWidth().get();
  r.h = body->getHeight().get();
  return r;
}

void Player::setPosition(Vec<int32_t> pos) {
  Body* body = Physics::get()->getBody(bodyId);
  body->setPosition(pos.x, pos.y);
}

int32_t Player::nextFireBall() {
  int32_t idF;
  if (queuefireBalls.size() == 0) {
    idF = fireBalls.size();
    fireBalls.push_back(FireBall());
  } else {
    idF = queuefireBalls.front();
    queuefireBalls.pop();
  }
  
  mapBuriedFireBalls[ idF ] = false;
  return idF;
}

