#ifndef _DOORVERTICAL_H_
#define _DOORVERTICAL_H_

#include "Resources.h"
#include "Animator.h"
#include "Physics.h"

class DoorVertical {
public:
  DoorVertical();
  ~DoorVertical();

  void init(Rect<int32_t> bounds);
  void update(float deltatime, bool emblemOn);
  void draw();

  Rect<int32_t> bounds;
  int32_t bodyId;

  int32_t limitTop;
  int32_t limitBot;
};


#endif
