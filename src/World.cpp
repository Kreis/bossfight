#include "World.h"

World::World() {}
World::~World() {}

void World::start() {

  std::cout << "World start" << std::endl;

  Vec<int32_t> screenSize;
  screenSize.x = 1400;
  screenSize.y = 760;
  window.start("Boss Battle", screenSize.x, screenSize.y);
  Camera::get()->bounds.w = screenSize.x;
  Camera::get()->bounds.h = screenSize.y;

  GlobalConfig::get()->originalScreenSize = screenSize;

  window.setIcon("data/textures/icon.png");

  Resources::get()->loadTextureAtlas("data/textures/GlobalTexture.json");

  // Add All the texture will be used in the game
  std::vector<std::string> texturePriority;
  texturePriority.push_back("FIREWORKS");
  texturePriority.push_back("data/textures/Tiles.png");
  texturePriority.push_back("data/textures/GlobalTexture.png");
  texturePriority.push_back("data/textures/arial25.png");
  texturePriority.push_back("BANNERS");
  SpriteBatch::get()->init(texturePriority);
  SpriteBatch::get()->setNullTextureVecVertex("BANNERS");
  SpriteBatch::get()->setCameraFreeVecVertex("BANNERS", true);
  SpriteBatch::get()->setNullTextureVecVertex("FIREWORKS");

  Scene scene;
  scene.init();

  sf::Clock clock;
  float frameRateTime = 0;
  int32_t frameRate = 0;
  std::cout << "start loop" << std::endl;
  while (window.isOpen()) {

    window.update();

//    if (InputState::get()->isTyped(KeyButton::W)) {
//      window.close();
//      break;
//    }

    auto clamp = [](float a, float b, float v) {
      if (v < a) return a;
      if (v > b) return b;
      return v;
    };
    if (InputState::get()->isTyped(KeyButton::Add)) {
      GlobalConfig::get()->volume = clamp(0.0, 1.0, GlobalConfig::get()->volume + 0.1);
      std::cout << "set voluem as " << GlobalConfig::get()->volume << std::endl;
    }
    if (InputState::get()->isTyped(KeyButton::Subtract)) {
      GlobalConfig::get()->volume = clamp(0.0, 1.0, GlobalConfig::get()->volume - 0.1);
      std::cout << "set voluem as " << GlobalConfig::get()->volume << std::endl;
    }

    if (GlobalConfig::get()->exitGame) {
      std::cout << "exit game" << std::endl;
      window.close();
      break;
    }

    if (InputState::get()->isResized()) {
      std::cout << "resized" << std::endl;     
      std::cout << InputState::get()->getCurrentScreenSize().x << std::endl;
      std::cout << InputState::get()->getCurrentScreenSize().y << std::endl;
    }
  
    float elapsedTime = clock.getElapsedTime().asSeconds();
    clock.restart();
    scene.update(elapsedTime, window.getSfRenderWindow());
    scene.draw();
    SpriteBatch::get()->flush(&window);

    frameRate++;
    frameRateTime += elapsedTime;

    if (frameRateTime >= 1.0) {
      std::cout << "FPS: " << frameRate << std::endl;
      frameRate = 0;
      frameRateTime = 0;
    }
  }
  

}

