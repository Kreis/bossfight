#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

#include "sharedtypes.h"

class Sprite {
public:
  Sprite();
  ~Sprite();

  std::string textureName;
  Rect<int32_t> bounds;
  Rect<int32_t> textureRect;
  bool horizontalSwitch;
  bool verticalSwitch;
  float rotation;

  Rect<int32_t> color;
 
};


#endif
