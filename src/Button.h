#ifndef _BUTTON_H_
#define _BUTTON_H_

#include <iostream>

#include "SpriteBatch.h"
#include "GlobalConfig.h"
#include "Animator.h"
#include "Resources.h"
#include "Physics.h"
#include "sharedtypes.h"
#include "Player.h"
#include "Camera.h"

class Button {
public:
  Button();
  ~Button();

  void init(Rect<int32_t> rect);
  void update(float deltatime, Player* player);
  void draw();

  void push();
  void unpush();


  bool pushed;
  uint32_t body;
  Rect<int32_t> buttonBounds;

  float currentShaking;
  float timeShaking;
  Vec<float> cameraMoved;
  int32_t cameraMovedId;

};

#endif
