#include "GraphAnimator.h"


GraphAnimator::GraphAnimator() {}
GraphAnimator::~GraphAnimator() {}

std::string GraphAnimator::getUpdatedAnimation(
    const std::map<std::string, std::string>& status) {

  visited[ nodeId ] = true;
  int32_t toNode = -1;
  for (int32_t i = 0; i < nodes[ nodeId ].nodeAnimators.size(); i++) {
    const std::vector<std::map<std::string, std::string> >& eachConditions =
      nodes[ nodeId ].conditions[ i ];

    for (const std::map<std::string, std::string>& anyCondition :
      eachConditions) {

      toNode = nodes[ nodeId ].nodeAnimators[ i ];
      bool atLeastOne = false;
      for (auto it = anyCondition.begin(); it != anyCondition.end(); it++) {
        if (status.at(it->first) == it->second) {
          atLeastOne = true;
          break;
        }
      }

      // if no condition success this is not the way
      if ( ! atLeastOne) {
        toNode = -1;
        break;
      }
    }

    // if toNode already has a way, no need to look for another
    if (toNode != -1) {
      break;
    }
  }

  // if no direction found or next node was already visited (loop),
  // return same animation name
  if (toNode == -1 || visited[ toNode ]) {
    // reset
    std::fill(visited.begin(), visited.end(), false);
    return nodes[ nodeId ].animationName;
  }

  // return recusively next animation in the graph
  nodeId = toNode;
  return getUpdatedAnimation(status);
}






