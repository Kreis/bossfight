#include "Door.h"

Door::Door() {}

Door::~Door() {}

void Door::init(Rect<int32_t> bounds) {

  Resources::get()->loadAnimator("data/animators/door01.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/door01.json");
  animator->runAnimation("idle");
  this->bounds = bounds;

  animator->bounds = bounds;

  body = Physics::get()->generateBody(
    bounds.x,
    bounds.y,
    bounds.w,
    bounds.h,
    /*isDynamic=*/true);
  Body* b = Physics::get()->getBody(body);
  b->setType(9);

  limitLeft = bounds.x;
  limitRight = bounds.x + bounds.w;
}

void Door::update(float deltatime, bool buttonPushed) {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/door01.json");

  animator->update(deltatime);

  Body* b = Physics::get()->getBody(body);
  float progress = 250.0 * deltatime;
  if (buttonPushed) {
    b->move(progress, 0);
    if (b->getX() > limitRight) {
      b->setPositionX(limitRight);
    }
  } else {
    b->move(-progress, 0);
    if (b->getX() < limitLeft) {
      b->setPositionX(limitLeft);
    }
  }
}

void Door::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/door01.json");

  Body* b = Physics::get()->getBody(body);
  animator->bounds.x = b->getX().get();
  animator->draw();
}





