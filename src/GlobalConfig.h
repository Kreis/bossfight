#ifndef _GLOBAL_CONFIG_H_
#define _GLOBAL_CONFIG_H_

#include <memory>

#include "sharedtypes.h"

class GlobalConfig {
public:
  ~GlobalConfig();

  static GlobalConfig* get();

  bool exitGame = false;
  float volume = 0.6;

  Vec<int32_t> originalScreenSize;

private:
  GlobalConfig();
  static std::unique_ptr<GlobalConfig> instancePtr;
};



#endif
