#include "StageTraining.h"

StageTraining::StageTraining() {}
StageTraining::~StageTraining() {}

void StageTraining::init(Player* player) {
  name = "data/maps/trainingRoom.json";
  nextStageName = name;

  std::cout << "init stage " << name << std::endl;

  Resources::get()->loadMap(name);
  Map* map = Resources::get()->getMap(name);

  SpriteBatch::get()->fixVecVertex(
    map->tilesetTextureName,
    map->getVecVertex());


  Rect<int32_t> startSpawn;
  Rect<int32_t> buttonSpawn;
  Rect<int32_t> door01Spawn;
  Rect<int32_t> rectNextStage;
  Rect<int32_t> rectFireWorks;
  for (int32_t i = 0; i < map->objectsName.size(); i++) {
    std::cout << "iterate object "
      << map->objectsName[ i ] << std::endl;
    if (map->objectsName[ i ] == "physics") {
      std::cout << "is physics" << std::endl;
      int32_t ptmp = Physics::get()->generateBody(
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y,
        map->objectsRect[ i ].w,
        map->objectsRect[ i ].h);
      Physics::get()->getBody(ptmp)->setType(9);
      bodies.push_back(ptmp);
    }


    if (map->objectsName[ i ] == "startSpawn") {
      startSpawn = map->objectsRect[ i ];
    }
    if (map->objectsName[ i ] == "button") {
      buttonSpawn = map->objectsRect[ i ];
    }
    if (map->objectsName[ i ] == "door01") {
      door01Spawn = map->objectsRect[ i ];
    }

    if (map->objectsName[ i ] == "torch01") {
      Vec<int32_t> pos = {
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y };
      fe01.init(pos);     
    }
    if (map->objectsName[ i ] == "torch02") {
      Vec<int32_t> pos = {
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y };
      fe02.init(pos);     
    }
    if (map->objectsName[ i ] == "torch03") {
      Vec<int32_t> pos = {
        map->objectsRect[ i ].x,
        map->objectsRect[ i ].y };
      fe03.init(pos);     
    }
    if (map->objectsName[ i ] == "door02") {
      dv01.init(map->objectsRect[ i ]);
    }
    if (map->objectsName[ i ] == "door03") {
      dv02.init(map->objectsRect[ i ]);
    }
    if (map->objectsName[ i ] == "door04") {
      dv03.init(map->objectsRect[ i ]);
    }

    if (map->objectsName[ i ] == "nextStage") {
      rectNextStage = map->objectsRect[ i ];
    }
    if (map->objectsName[ i ] == "fireworks") {
      rectFireWorks = map->objectsRect[ i ];
    }

  }

  player->setPosition({ startSpawn.x, startSpawn.y });

  // button
  button.init(buttonSpawn);

  // door
  door.init(door01Spawn);

  cameraSpeed = 2000.0;

  // music
  thisVolume = GlobalConfig::get()->volume;
  Resources::get()->loadMusic("data/sounds/background.ogg");
  sf::Music* music =
    Resources::get()->getMusic("data/sounds/background.ogg");
  music->setVolume(thisVolume * thisVolume * 0.5 * 100.0); 
  music->setLoop(true);
  music->play();

  // fireworks
  fireWorks.init(rectNextStage, rectFireWorks);

}

void StageTraining::update(float deltatime, Player* player) {

  float vol = GlobalConfig::get()->volume;
  if (thisVolume != vol) {
    Resources::get()->getMusic("data/sounds/background.ogg")->
      setVolume(vol * vol * 0.5 * 100.0);
  }

  Physics::get()->update();

  player->update(deltatime);
  Rect<int32_t> r = player->getBounds();


  float fromX = Camera::get()->bounds.x;
  float fromY = Camera::get()->bounds.y;

  float goingX =
    r.x + r.w / 2 - GlobalConfig::get()->originalScreenSize.x / 2;
  float goingY =
    r.y + r.h / 2 - 4 * GlobalConfig::get()->originalScreenSize.y / 5;

  float distance = sqrt(
    (goingX - fromX) * (goingX - fromX) +
    (goingY - fromY) * (goingY - fromY));
  
  float progres = cameraSpeed * deltatime;
  if (progres > distance || distance < 32) {
    Camera::get()->bounds.x = goingX;
    Camera::get()->bounds.y = goingY;
  } else {
    float perc = progres / distance;
    Camera::get()->bounds.x = fromX + perc * (goingX - fromX);
    Camera::get()->bounds.y = fromY + perc * (goingY - fromY);
  }
    

  button.update(deltatime, player);
  door.update(deltatime, button.pushed);

  fe01.update(deltatime, player->fireBalls);
  fe02.update(deltatime, player->fireBalls);
  fe03.update(deltatime, player->fireBalls);

  dv01.update(deltatime, fe01.on);
  dv02.update(deltatime, fe02.on);
  dv03.update(deltatime, fe03.on);

  fireWorks.update(deltatime, player);
}

void StageTraining::draw(Player* player) {

  player->draw();
  button.draw();
  door.draw();
  fe01.draw();
  fe02.draw();
  fe03.draw();
  dv01.draw();
  dv02.draw();
  dv03.draw();

  fireWorks.draw();
}

void StageTraining::off(Player* player) {
  std::cout << "off stage training" << std::endl;
  SpriteBatch::get()->clearVecVertex("data/textures/Tiles.png");

  for (int32_t i : bodies) {
    Physics::get()->getBody(i)->off();
  }


}

