#ifndef SPIRAL_H_
#define SPIRAL_H_

#include "sharedtypes.h"
#include "Resources.h"
#include "Animator.h"
#include "SpriteBatch.h"
#include "FireBall.h"
#include "Physics.h"

class Spiral {
public:
  Spiral();
  ~Spiral();

  void init();
  void putIt(Vec<float> pos);
  Vec<int32_t> getIt();
  void update(float deltatime, std::vector<FireBall>& fbs);
  void draw();

  Rect<int32_t> bounds;
  bool active;

  int32_t bodyIdSpace;
};

#endif
