#ifndef _STAGETRAINING_H_
#define _STAGETRAINING_H_

#include <iostream>

#include "StageGeneric.h"
#include "GlobalConfig.h"
#include "FireEmblem.h"
#include "Player.h"
#include "Button.h"
#include "Door.h"
#include "FireWorks.h"
#include "DoorVertical.h"

class StageTraining : public StageGeneric {
public:
  StageTraining();
  ~StageTraining();
  void init(Player* player);
  void update(float deltatime, Player* player);
  void draw(Player* player);
  void off(Player* player);

  std::vector<uint32_t> bodies;

  Button button;
  Door door;
  FireEmblem fe01;
  FireEmblem fe02;
  FireEmblem fe03;
  DoorVertical dv01;
  DoorVertical dv02;
  DoorVertical dv03;

  FireWorks fireWorks;

  float cameraSpeed;

  float thisVolume;
};

#endif
