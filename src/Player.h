#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <iostream>
#include <queue>

#include "Animator.h"
#include "Resources.h"
#include "Physics.h"
#include "Camera.h"
#include "Parabola.h"
#include "GlobalConfig.h"
#include "FireBall.h"
#include "Spiral.h"

class Player {
public:
  Player();
  ~Player();

  void init();
  void update(float deltatime);
  void draw();

  Rect<int32_t> getBounds();
  void setPosition(Vec<int32_t> pos);

  uint32_t bodyId;

  Parabola parabola;
  bool jumping;
  bool falling;
  bool fireBall;
  bool down;
  bool flip;

  std::map<std::string, std::string> animatorStatus;
  std::vector<FireBall> fireBalls;
  std::queue<int32_t> queuefireBalls;
  std::map<int32_t, bool> mapBuriedFireBalls;
  int32_t nextFireBall();
  float fireBallDelay = 0.3;
  float currentFireBallDelay = 0.0;

  Spiral spiral;
  bool reappeared;
};


#endif
