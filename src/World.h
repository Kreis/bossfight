#ifndef _WORLD_H_
#define _WORLD_H_

#include <iostream>
#include <functional>

#include "sharedtypes.h"
#include "SpriteBatch.h"
#include "Player.h"
#include "Window.h"
#include "Resources.h"
#include "Scene.h"
#include "GlobalConfig.h"

class World {
public:
  World();
  ~World();

  void start();

  Window window;

};


#endif
