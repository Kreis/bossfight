#ifndef _LETTER_H_
#define _LETTER_H_

#include <iostream>
#include <memory>

#include "sharedtypes.h"

class Letter {
public:
	Letter(
    char letter,
    int32_t x,
    int32_t y,
    int32_t width,
    int32_t height,
    int32_t xoffset,
    int32_t yoffset,
    int32_t xadvance,
    std::string texture_name);

	char letter;
	Rect<int32_t> texture_rect;
	int32_t xoffset;
	int32_t yoffset;
	int32_t xadvance;
	std::string texture_name;
};

#endif
