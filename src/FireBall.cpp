#include "FireBall.h"

FireBall::FireBall() {}
FireBall::~FireBall() {}

void FireBall::init(Vec<int32_t> position, Vec<float> _direction) {

  std::cout << "Fire ball init" << std::endl;
  Resources::get()->loadAnimator("data/animators/fireball.json");
  Animator* animator =
    Resources::get()->getAnimator("data/animators/fireball.json");

  bodyId = Physics::get()->generateBody(
    position.x,
    position.y,
    animator->bounds.w,
    animator->bounds.h,
    /*isDynamic=*/true);

  Physics::get()->getBody(bodyId)->setType(1);

  direction = _direction;
  alive = true;
  sessionId = animator->getNewSession();
  animator->runAnimation("goingOff", sessionId);

  Resources::get()->loadSound("data/sounds/fireBall.wav");
  float vol = GlobalConfig::get()->volume;
  Resources::get()->getSound("data/sounds/fireBall.wav")->setVolume(vol * vol * 100.0);
  Resources::get()->getSound("data/sounds/fireBall.wav")->play();
}

void FireBall::updateAndDraw(float deltatime) {
  if ( ! alive) {
    return;
  }

  Vec<float> npos = direction * deltatime;

  Body* body = Physics::get()->getBody(bodyId);
  body->move(npos.x, npos.y);

  Animator* animator =
    Resources::get()->getAnimator("data/animators/fireball.json");
  animator->bounds.x = (int32_t)body->getX().get();
  animator->bounds.y = (int32_t)body->getY().get();
  if (direction.x < 0) {
    animator->setHorizontalSwitch(true, sessionId);
  }
  if (direction.x > 0 && direction.y < 0) {
    animator->setRotation(-3.141592 / 4.0, sessionId);
  }
  if (direction.x < 0 && direction.y < 0) {
    animator->setRotation(3.141592 / 4.0, sessionId);
  }
  if (direction.x == 0 && direction.y < 0) {
    animator->setRotation(-3.141592 / 2.0, sessionId);
  }

  // if collide
  if (body->isColliding()) {
    animator->runAnimation("end", sessionId);
  }

  animator->update(deltatime, sessionId);

  // if animation end finish, delete this object
  if (animator->getStoped(sessionId)) {
    body->off();
    alive = false;
    return;
  }

  animator->draw(sessionId);
}

Rect<int32_t> FireBall::getBounds() {
  Body* body = Physics::get()->getBody(bodyId);
  return {
    (int32_t)body->getX().get(),
    (int32_t)body->getY().get(),
    (int32_t)body->getWidth().get(),
    (int32_t)body->getHeight().get(),
  };
}




