#include "Scene.h"

Scene::Scene() {}
Scene::~Scene() {}

void Scene::init() {

  stages["data/map/trainingRoom.json"] = std::make_unique<StageTraining>();
  currentStage = stages["data/map/trainingRoom.json"].get();

  player = std::make_unique<Player>();
  player->init();

  currentStage->init(player.get());

  banner1.textureName = "BANNERS";
  banner2.textureName = "BANNERS";
  banner1.color = {0, 0, 0, 255};
  banner2.color = {0, 0, 0, 255};
//  banner1.textureRect = {0, 0, 1, 1};
//  banner2.textureRect = {0, 0, 1, 1};
  drawBanners = false;
  currentScreenSize = GlobalConfig::get()->originalScreenSize;
}

void Scene::update(float deltatime, sf::RenderWindow* rwindow) {

  // verify change map
  if (currentStage->nextStageName.length() > 0 &&
    currentStage->nextStageName != currentStage->name) {

    currentStage->off(player.get());
    if (stages[ currentStage->nextStageName ] == nullptr) {
      std::cout << "ERROR: Next map does not exists" << std::endl;
    }

    currentStage = stages[ currentStage->nextStageName ].get();
    currentStage->init(player.get());
  }

  currentStage->update(deltatime, player.get());

  Vec<int32_t> newScreenSize = InputState::get()->getCurrentScreenSize();
  if (newScreenSize != currentScreenSize) {
    
    float xx = (float)GlobalConfig::get()->originalScreenSize.x;
    float yy = (float)GlobalConfig::get()->originalScreenSize.y;
    static float oratio = xx / yy;

    float nratio = (float)newScreenSize.x / (float)newScreenSize.y;
    float invnratio = (float)newScreenSize.y / (float)newScreenSize.x;

    sf::FloatRect frect;
    frect.left = 0;
    frect.top = 0;

    // if needs left right banners
    if (nratio > oratio) {
      Camera::get()->banners.x = (int32_t)((yy * nratio - xx) / 2.0);
      Camera::get()->banners.y = 0;
      frect.width = yy * nratio;
      frect.height = yy;

      banner1.bounds.w = Camera::get()->banners.x;
      banner1.bounds.h = yy;

      banner2.bounds.x = Camera::get()->banners.x + xx;
      banner2.bounds.y = 0;
      banner2.bounds.w = Camera::get()->banners.x + 2;
      banner2.bounds.h = yy;

      drawBanners = true;
    } else if (nratio < oratio) {
      // otherwise, top bot banners
      Camera::get()->banners.x = 0;
      Camera::get()->banners.y = (int32_t)((xx * invnratio - yy) / 2.0);

      frect.width = xx;
      frect.height = xx * invnratio;

      banner1.bounds.w = xx;
      banner1.bounds.h = Camera::get()->banners.y;

      banner2.bounds.x = 0;
      banner2.bounds.y = yy + Camera::get()->banners.y;
      banner2.bounds.w = xx;
      banner2.bounds.h = Camera::get()->banners.y + 2;

      drawBanners = true;
    } else {
      Camera::get()->banners.x = 0;
      Camera::get()->banners.y = 0;

      drawBanners = false;
    }

    sf::View view(frect);
    rwindow->setView(view);

    currentScreenSize = newScreenSize;
  }

}

void Scene::draw() {

  currentStage->draw(player.get());

  if (drawBanners) {
    SpriteBatch::get()->drawSprite(&banner1);
    SpriteBatch::get()->drawSprite(&banner2);
  }
}






