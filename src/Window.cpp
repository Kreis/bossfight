#include "Window.h"

Window::Window() {}
Window::~Window() {}

void Window::start(std::string name, int32_t width, int32_t height) {

  std::cout << "Window start" << std::endl;
  sfRenderWindow = std::make_unique<sf::RenderWindow>(
    sf::VideoMode(width, height), name);

  sfRenderWindow->setVerticalSyncEnabled(true);
//  sfRenderWindow->setFramerateLimit(60);
}

void Window::update() {

  if (sfRenderWindow == nullptr) {
    std::cout << "ERROR: sf render window not initialized" << std::endl;
    return;
  }

  InputState::get()->updateState(sfRenderWindow.get());

  if (InputState::get()->closed) {
    sfRenderWindow->close();
  }
}


bool Window::isOpen() {

  return sfRenderWindow->isOpen();

}

void Window::setIcon(std::string name) {
  std::cout << "window set icon" << std::endl;
  if ( ! icon.loadFromFile(name)) {
    std::cout << "ERROR: icon window not found in " << name << std::endl;
  }
  sfRenderWindow->
    setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
}

void Window::close() {
  sfRenderWindow->close();
}


sf::RenderWindow* Window::getSfRenderWindow() {
  return sfRenderWindow.get();
}




