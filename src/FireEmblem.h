#ifndef _FIREMBLEM_H_
#define _FIREMBLEM_H_

#include <iostream>
#include <vector>

#include "Resources.h"
#include "Animator.h"
#include "sharedtypes.h"
#include "FireBall.h"
#include "GlobalConfig.h"

class FireEmblem {
public:
  FireEmblem();
  ~FireEmblem();

  void init(Vec<int32_t> pos);
  void update(float deltatime, std::vector<FireBall>& fbs);
  void draw();

  Vec<int32_t> pos;
  Rect<int32_t> bounds;

  bool on;
  float delayOn;
  int32_t sessionId;
};


#endif
