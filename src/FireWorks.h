#ifndef _FIREWORKS_H_
#define _FIREWORKS_H_

#include <iostream>

#include "Resources.h"
#include "SpriteBatch.h"
#include "Camera.h"
#include "sharedtypes.h"
#include "Player.h"

class FireWorks {
public:
  FireWorks();
  ~FireWorks();

  void init(Rect<int32_t> goal, Rect<int32_t> rectFireWorks);
  void update(float deltatime, Player* player); 
  void draw();

  bool win;
  Rect<int32_t> bounds;
  Rect<int32_t> goalBounds;

  Sprite sprite;
  float time;
};

#endif
